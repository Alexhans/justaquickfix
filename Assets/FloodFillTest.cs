﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Color = UnityEngine.Color;

namespace FloodFill
{
    class FloodFillTest
    {
        private static bool ColorMatch(Color a, Color b)
        {
            return a == b;
        }

        public static void FloodFill(Texture2D bmp, Vector2Int pt, Color targetColor, 
            UnityEngine.Color replacementColor)
        {
            Queue<Vector2Int> q = new Queue<Vector2Int>();
            q.Enqueue(pt);
            Vector2Int n = q.Dequeue(); ;
            
            while (q.Count > 0) { 

                n = q.Dequeue();

                if (!ColorMatch(bmp.GetPixel(n.x, n.y),targetColor))
                    continue;
                Vector2Int w = n, e = new Vector2Int(n.x + 1, n.y);
                while ((w.x >= 0) && ColorMatch(bmp.GetPixel(w.x, w.y),targetColor))
                {
                    bmp.SetPixel(w.x, w.y, replacementColor);
                    if ((w.y > 0) && ColorMatch(bmp.GetPixel(w.x, w.y - 1),targetColor))
                        q.Enqueue(new Vector2Int(w.x, w.y - 1));
                    if ((w.y < bmp.height - 1) && ColorMatch(bmp.GetPixel(w.x, w.y + 1),targetColor))
                        q.Enqueue(new Vector2Int(w.x, w.y + 1));
                    w.x--;
                }
                while ((e.x <= bmp.width - 1) && ColorMatch(bmp.GetPixel(e.x, e.y),targetColor))
                {
                    bmp.SetPixel(e.x, e.y, replacementColor);
                    if ((e.y > 0) && ColorMatch(bmp.GetPixel(e.x, e.y - 1), targetColor))
                        q.Enqueue(new Vector2Int(e.x, e.y - 1));
                    if ((e.y < bmp.height - 1) && ColorMatch(bmp.GetPixel(e.x, e.y + 1), targetColor))
                        q.Enqueue(new Vector2Int(e.x, e.y + 1));
                    e.x++;
                }
            }
        }
 
        /*static void Main(string[] args)
        {
            Bitmap bmp = new Bitmap("Unfilledcirc.bmp");
            FloodFill(bmp, new Point(200, 200), Color.White, Color.Red);
            FloodFill(bmp, new Point(100, 100), Color.Black, Color.Blue);
            bmp.Save("Filledcirc.bmp");
        }*/
    }
}