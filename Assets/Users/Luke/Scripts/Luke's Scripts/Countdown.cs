﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Countdown : MonoBehaviour
{
    private float trackedTime = 120;
    private float timeToBeWorried = 120;
    public Text textBox;

    public Level level;

    // TODO reasons for loss    
    public delegate void LoseLevel();
    public static event LoseLevel OnLoseLevel;

    public delegate void CloseToLosing();
    public static event CloseToLosing OnCloseToLosing;

    // Start is called before the first frame update
    void Start()
    {

        trackedTime = level.gameObject.GetComponent<Level>().timeoutToLoseInSeconds;
        timeToBeWorried = level.gameObject.GetComponent<Level>().timeoutToBeWorriedInSeconds;
        textBox.text = trackedTime.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        trackedTime -= Time.deltaTime;
        textBox.text = Mathf.Round(trackedTime).ToString();

        //if(Time less or equal to zero )
            //SendGameOverNotification()
        if(trackedTime < timeToBeWorried)
        {
            OnCloseToLosing?.Invoke();
            timeToBeWorried *= 0.5f;
        }
        if(trackedTime < 0)
        {
            OnLoseLevel?.Invoke();
            StartCoroutine(GameOverCoroutine());
        }
    }

    IEnumerator GameOverCoroutine()
    {
        //Event: Dog gets very Upset
        //Event: Sad noise plays
        //Scene switches to GameOver
        //Requires Yield New Return
        //Wait 3 Seconds
        Debug.Log("Darn it! Game Over!");

        trackedTime = 0;

        yield return new WaitForSeconds(3);

        SceneManager.LoadScene("Defeat");
        //ChangeSceneToGameOverScene;
    }
}
