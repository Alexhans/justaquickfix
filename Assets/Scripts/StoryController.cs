﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StoryController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void GoToNextStorySceneOrGame()
    {
        int targetLevel = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene(targetLevel);
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            GoToNextStorySceneOrGame();
        }
    }
}
