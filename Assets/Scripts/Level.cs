﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    public enum Tool
    {
        Eraser,
        Brush,
        Bucket,
        Treat
    };

    private float secondsAfterLevelVictory = 5.0f;
    private float secondsAfterDefeat = 5.0f;

    public Tool startingTool = Tool.Bucket;
    private Tool currentTool;
    public int currentTime;
    public AudioClip levelSong;

    [SerializeField] private float thresholdToWin = 70.0f;

    /* If you increase X in less than Y time then happy.
     * If you decrese X in less than Y time then sad. */
    // [SerializeField] private float thresholdToBeHappy = 10.0f;
    // [SerializeField] private float thresholdToBeSad = 10.0f;


    [Tooltip("When this amount of time elapses, you'll lose the level.")]
    public int timeoutToLoseInSeconds = 60;
    [Tooltip("When there's this amount of time remaining, pet will switch to worried look")]
    public int timeoutToBeWorriedInSeconds = 20;

    [Tooltip("Level of light that the original painting has that must be matched")]
    [Range(0.1f, 1.0f)]
    public float targetLevelOfLight = 1.0f;

    [Tooltip("Increments/Decrements when pressing +/-")]
    [Range(0.1f, 0.25f)]
    public float lightIncrements = 0.1f;

    public delegate void WinLevel(float affinity);
    public static event WinLevel OnWinLevel;

    void OnEnable()
    {
        DrawMe.OnAffinityUpdated += checkAffinity;
        Level.OnWinLevel += HandleLevelWon;
        Countdown.OnLoseLevel += HandleLevelLost;
        SelectTool.OnToolSelected += selectTool;
        SelectTool.OnModifyDimmer += modifyDimmer;
        SelectTool.OnModifyBrushSize += modifyBrushSize;
    }

    void OnDisable()
    {
        DrawMe.OnAffinityUpdated -= checkAffinity;
        Level.OnWinLevel -= HandleLevelWon;
        Countdown.OnLoseLevel -= HandleLevelLost;
        SelectTool.OnToolSelected -= selectTool;
        SelectTool.OnModifyDimmer -= modifyDimmer;
        SelectTool.OnModifyBrushSize -= modifyBrushSize;
    }

    private void checkAffinity(float factor)
    {
        Debug.Log(String.Format("Affinity: {0}", factor));
        if (factor > thresholdToWin)
        {
            OnWinLevel?.Invoke(factor);
        }
    }

    public void selectTool(string tool)
    {
        //currentTool = tool;
        Debug.Log($"Select current tool {tool}");

        if (tool == "Bucket") this.currentTool = Tool.Bucket;
        else if (tool == "Brush") this.currentTool = Tool.Brush;
        else if (tool == "Eraser") this.currentTool = Tool.Eraser;
        else if (tool == "Treat") this.currentTool = Tool.Treat;
    }

    public void modifyDimmer(bool up)
    {
        Debug.Log($"Modify dimmer: {up}");
    }

    public void modifyBrushSize(bool up)
    {
        Debug.Log($"Modify brush: {up}");
    }
    // Start is called before the first frame update
    void Start()
    {
        currentTime = timeoutToLoseInSeconds;
        currentTool = startingTool;
    }

    void HandleLevelWon(float factor)
    {
        StartCoroutine(WaitAndMoveToNextLevel(factor));
    }

    void HandleLevelLost()
    {
        StartCoroutine(WaitAndMoveToGameOver());
    }

    IEnumerator WaitAndMoveToNextLevel(float factor)
    {
        Debug.Log(String.Format("Level won with Affinity {0}, wait for a while", factor));
        yield return new WaitForSeconds(secondsAfterLevelVictory);

        GoToNextScene();
    }

    IEnumerator WaitAndMoveToGameOver()
    {
        Debug.Log("Game over! wait for a while");
        yield return new WaitForSeconds(secondsAfterDefeat);

        SceneManager.LoadScene("Defeat");
    }

    public void GoToNextScene()
    {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (sceneIndex + 1 < SceneManager.sceneCountInBuildSettings)
        {
            sceneIndex += 1;
            StartCoroutine(LoadYourAsyncScene(sceneIndex));
        }
    }

    IEnumerator LoadYourAsyncScene(int sceneIndex)
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneIndex);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}