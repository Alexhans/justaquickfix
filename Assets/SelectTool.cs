﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectTool : MonoBehaviour
{
    public delegate void ClickSelectTool(string toolName);
    public static event ClickSelectTool OnToolSelected;

    public delegate void ModifyDimmer(bool Up);
    public static event ModifyDimmer OnModifyDimmer;

    public delegate void ModifyBrushSize(bool Up);
    public static event ModifyBrushSize OnModifyBrushSize;

    public void NotifyOfToolChange(string toolName)
    {
        OnToolSelected?.Invoke(toolName);
    }

    public void NotifyOfDimmerChange(bool up)
    {
        OnModifyDimmer?.Invoke(up);
    }

    public void NotifyOfBrushChange(bool up)
    {
        OnModifyBrushSize?.Invoke(up);
    }
}
