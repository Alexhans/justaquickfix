﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DrawMe : MonoBehaviour
{
    // TODO: Detect this.
    private int paintingWidth = 600;
    private int paintingHeight = 800;

    // TODO Remove this scaling factor and derive it from the actual thing
    private float originalScaleFactor = 0.5f;

    public int brushSizeIncrements = 6;
    private int halfbrushsize = 6;

    public Texture2D damagedPainting;
    public Texture2D originalPainting;

    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    private Texture2D actingTexture;

    public Color pickedColor = Color.red;

    public delegate void UpdateAffinity(float factor);
    public static event UpdateAffinity OnAffinityUpdated;

    public Level.Tool startingTool = Level.Tool.Bucket;
    private Level.Tool currentTool;

    private Color[] brushMask;

    void OnEnable()
    {
        GraphicRaycasterRaycasterExample.OnRaycastedPainting += MyColorDrawDamagedPainting;
        GraphicRaycasterRaycasterExample.OnRaycastedOriginalPainting += PickColorFromOriginalPainting;
        SelectTool.OnModifyDimmer += modifyDimmer;
        SelectTool.OnModifyBrushSize += modifyBrushSize;
        SelectTool.OnToolSelected += selectTool;
    }

    private void modifyBrushSize(bool up)
    {
        int modifier = brushSizeIncrements;
        if (!up) modifier *= -1;
        this.halfbrushsize = Mathf.Clamp(10, this.halfbrushsize + modifier, 100);
        RegenerateBrush();
    }

    private void modifyDimmer(bool up)
    {
        throw new NotImplementedException();
    }

    void OnDisable()
    {
        GraphicRaycasterRaycasterExample.OnRaycastedPainting -= MyColorDrawDamagedPainting;
        GraphicRaycasterRaycasterExample.OnRaycastedOriginalPainting -= PickColorFromOriginalPainting;
        SelectTool.OnModifyDimmer -= modifyDimmer;
        SelectTool.OnModifyBrushSize -= modifyBrushSize;
        SelectTool.OnToolSelected -= selectTool;
    }

    private void selectTool(string tool)
    {
        if (tool == "Bucket") currentTool = Level.Tool.Bucket;
        else if (tool == "Brush") {
            currentTool = Level.Tool.Brush;
            pickedColor = Color.black;
        }
        else if (tool == "Eraser") {
            pickedColor = Color.white;
            currentTool = Level.Tool.Eraser;
          }
        else if (tool == "Treat") currentTool = Level.Tool.Treat;
    }

    void Teleport()
    {
        Vector3 pos = transform.position;
        pos.y = UnityEngine.Random.Range(1.0f, 3.0f);
        transform.position = pos;
    }

    void Start()
    {
        StartTextureWithDamagedPainting();
        currentTool = startingTool;
        RegenerateBrush();
    }


    private void RegenerateBrush()
    {
        this.brushMask = Enumerable.Repeat(pickedColor, halfbrushsize * 2 * halfbrushsize * 2).ToArray();
    }

    private void ChangeColorsOfBrush()
    {
        for(var i=0; i< this.brushMask.Length; i++)
        {
            if (this.brushMask[i] != Color.white) this.brushMask[i] = pickedColor;
        }
        //this.brushMask
        //this.brushMask = Enumerable.Repeat(pickedColor, halfbrushsize * 2 * halfbrushsize * 2).ToArray();
    }

    private void StartTextureWithDamagedPainting()
    {
        actingTexture = new Texture2D(paintingWidth, paintingHeight);

        Color color;
        for (int x = 0; x < paintingWidth; ++x)
        {
            for (int y = 0; y < paintingHeight; ++y)
            {

                color = damagedPainting.GetPixel(x, y);
                actingTexture.SetPixel(x, y, color);
            }
        }
        actingTexture.Apply();

        GetComponent<RawImage>().texture = actingTexture;

        CalculateAndNotifyAffinity();
    }

    public float GetPaintingAffinityWithOriginal()
    {
        float affinityCount = 0;
        int sampleCount = 0;
        for (int x = 0; x < paintingWidth; ++x)
        {
            for (int y = 0; y < paintingHeight; ++y)
            {

                if (originalPainting.GetPixel(x, y) == actingTexture.GetPixel(x, y))
                    affinityCount += 1;
                sampleCount += 1;
            }
        }

        return affinityCount / sampleCount;
    }
    private static void DrawSomething(Texture2D texture, Color colorOne, Color colorTwo)
    {
        for (int y = 0; y < texture.height; y++)
        {
            for (int x = 0; x < texture.width; x++)
            {
                Color color = ((x & y) != 0 ? colorOne : colorTwo);
                texture.SetPixel(x, y, color);
            }
        }
        texture.Apply();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            RandomColorDraw();
        }

    }
    private void PickColorFromOriginalPainting(Vector2 pos)
    {
        int x = Mathf.RoundToInt(pos.x * 1 / originalScaleFactor);
        int y = Mathf.RoundToInt(pos.y * 1 / originalScaleFactor);

        this.pickedColor = originalPainting.GetPixel(x, y);
        Debug.Log(String.Format("Picked Color: {0}", pickedColor.ToString()));

        ChangeColorsOfBrush();  
    }

    private void MyColorDrawDamagedPainting(Vector2 pos)
    {
        //Debug.Log("Mycolor draw ");
        Color[] colors = { Color.red, Color.green, Color.blue, Color.yellow };

        int x = Mathf.RoundToInt(pos.x);
        int y = Mathf.RoundToInt(pos.y);

        // jajajajajjaja    
        // for (int i = 0; i < paintingWidth; ++i)
        //     for (int j = 0; j < paintingHeight; ++j)

        if (this.currentTool == Level.Tool.Bucket)
        {
            BucketFill(x, y);
        }
        else if (this.currentTool == Level.Tool.Brush)
        {
            DrawOneShotBrush(x, y);
        }
        else if (this.currentTool == Level.Tool.Eraser)
        {
            // Ensure that the color is white if you have the eraser.
            pickedColor = Color.white;
            DrawOneShotBrush(x, y);
        }

        actingTexture.Apply();

        GetComponent<RawImage>().texture = actingTexture;

        CalculateAndNotifyAffinity();

    }

    private void BucketFill(int x, int y)
    {
        actingTexture.FloodFillArea(x, y, pickedColor);
        /*FloodFill.FloodFillTest.FloodFill(actingTexture,
            new Vector2Int(x, y),
            Color.white,
            pickedColor);*/
    }

    private void DrawOneShotBrush(int x, int y)
    {
        int startingX = x - halfbrushsize;
        int startingY = y - halfbrushsize;
        int brushX = halfbrushsize * 2;
        int brushY = halfbrushsize * 2;

        if (startingX < 0)
        {
            brushX += startingX;
            startingX = 0;
        }
        if (startingY < 0)
        {
            brushY += startingY;
            startingY = 0;
        }

        int width = Mathf.Min(this.paintingWidth - startingX, brushX);
        int height = Mathf.Min(this.paintingHeight - startingY, brushY);

        Color[] colors = Enumerable.Repeat(pickedColor, width * height).ToArray();

        actingTexture.SetPixels(
            startingX,
            startingY,
            width,
            height,
            colors);
    }

    private void CalculateAndNotifyAffinity()
    {
        float affinity = GetPaintingAffinityWithOriginal();
        Debug.Log(String.Format("Affinity: {0}", affinity));

        if (OnAffinityUpdated != null)
            OnAffinityUpdated(Mathf.Round(affinity * 100));
    }

    private void RandomColorDraw()
    {
        Color[] colors = { Color.red, Color.green, Color.blue, Color.yellow };
        Color randomPick = colors[UnityEngine.Random.Range(0, colors.Length)];

        DrawSomething(actingTexture, Color.white, randomPick);

        GetComponent<RawImage>().texture = actingTexture;
    }
}