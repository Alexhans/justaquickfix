﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowAffinityChange : MonoBehaviour
{
    private Text m_TextComponent;

    void OnEnable()
    {
        DrawMe.OnAffinityUpdated += UpdateAffinityLabel;
    }
    void OnDisable()
    {
        DrawMe.OnAffinityUpdated -= UpdateAffinityLabel;
    }

    private void UpdateAffinityLabel(float factor)
    {
        m_TextComponent = GetComponent<Text>();

        m_TextComponent.text = String.Format("Affinity: {0} %", factor);
    }


}
