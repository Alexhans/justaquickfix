﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class TextureExtentions
{
    public static Texture2D ToTexture2D(this Texture texture)
    {
        return Texture2D.CreateExternalTexture(
            texture.width,
            texture.height,
            TextureFormat.RGB24,
            false, false,
            texture.GetNativeTexturePtr());
    }
}

public class TestSomething : MonoBehaviour
{
    public GameObject ImageOnPanel;  ///set this in the inspector
    public Texture NewTexture;
    private RawImage img;

    Texture2D Paint2DTexture()
    {
        Texture2D myTexture2D = Texture2D.blackTexture; //img.texture.ToTexture2D();

        //myTexture2D.SetPixel(1, 1, Color.red);
        //myTexture2D.SetPixel(3, 3, Color.blue);
        myTexture2D.Apply();
        return myTexture2D;
    }
    // Start is called before the first frame update
    void Start()
    {
        img = (RawImage)ImageOnPanel.GetComponent<RawImage>();

        img.texture = (Texture)NewTexture;

        Texture2D myTexture2D = Paint2DTexture();
        img.texture = myTexture2D as Texture;

        Material material = GetComponent<Material>() as Material;

        material.mainTexture = myTexture2D;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
