﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopThroughSongs : MonoBehaviour
{
    public AudioClip[] songs;
    AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        PickRandomSong();
        audioSource = GetComponent<AudioSource>();
    }

    AudioClip PickRandomSong()
    {
        int songIndex = Random.Range(0, songs.Length);
        return songs[songIndex];
    }

    // Update is called once per frame
    void Update()
    {
        if(!audioSource.isPlaying)
        {
            PlayRandomSong();
        }
    }

    private void PlayRandomSong()
    {
        audioSource.clip = PickRandomSong();
        audioSource.Play();
    }
}
