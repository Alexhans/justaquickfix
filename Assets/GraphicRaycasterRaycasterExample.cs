﻿//Attach this script to your Canvas GameObject.
//Also attach a GraphicsRaycaster component to your canvas by clicking the Add Component button in the Inspector window.
//Also make sure you have an EventSystem in your hierarchy.

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class GraphicRaycasterRaycasterExample : MonoBehaviour
{
    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    public delegate void RayCastedPainting(Vector2 pos);
    public static event RayCastedPainting OnRaycastedPainting;

    public delegate void RayCastedOriginalPainting(Vector2 pos);
    public static event RayCastedOriginalPainting OnRaycastedOriginalPainting;

    public delegate void RayCastedDog(Vector2 pos);
    public static event RayCastedDog OnRaycastedDog;
    
    private int paintingWidth = 600;
    private int paintingHeight = 800;

    private float originalScaleFactor = 0.5f;
    private Level.Tool currentTool;

    void Start()
    {
        //Fetch the Raycaster from the GameObject (the Canvas)
        m_Raycaster = GetComponent<GraphicRaycaster>();
        //Fetch the Event System from the Scene
        m_EventSystem = GetComponent<EventSystem>();
    }

    private void OnEnable()
    {
        SelectTool.OnToolSelected += selectTool;
    }
    private void OnDis()
    {
        SelectTool.OnToolSelected -= selectTool;
    }
    private void selectTool(string tool)
    {
        if (tool == "Bucket") currentTool = Level.Tool.Bucket;
        else if (tool == "Brush")
        {
            currentTool = Level.Tool.Brush;
        }
        else if (tool == "Eraser")
        {
            currentTool = Level.Tool.Eraser;
        }
        else if (tool == "Treat") currentTool = Level.Tool.Treat;
    }
    private void GetLocalUICoordinates(RaycastResult result)
    {
        if(result.gameObject.name == "Dog")
        {
            Debug.Log("TEst");
            GetUICoordinates(result, out float x, out float y);
            OnRaycastedDog?.Invoke(new Vector2(x, y));
        }
        if(result.gameObject.name == "RawImage")
        {
            GetUICoordinates(result, out float x, out float y);

            OnRaycastedPainting?.Invoke(new Vector2(x, y));
        }
        else if (result.gameObject.name == "OriginalPainting")
        {
            GetUICoordinates(result, out float x, out float y, originalScaleFactor);

            OnRaycastedOriginalPainting?.Invoke(new Vector2(x, y));
        }
    }

    private void GetUICoordinates(RaycastResult result, out float x, out float y, float scaleFactor=1.0f)
    {
        float objectLengthX = paintingWidth * scaleFactor;
        float objectEdgeX = result.gameObject.transform.position.x - objectLengthX / 2;
        x = result.screenPosition.x - objectEdgeX;
        float objectLengthY = paintingHeight * scaleFactor;
        float objectEdgeY = result.gameObject.transform.position.y - objectLengthY / 2;
        y = result.screenPosition.y - objectEdgeY;

        /*
        Debug.Log(string.Format("objectEdgeX {0}", objectEdgeX));
        Debug.Log(string.Format("Screen pos {0}", result.screenPosition.x));
        Debug.Log(string.Format("gameObject pos {0}", result.gameObject.transform.position.x));
        Debug.Log(string.Format("gameObject local pos {0}", result.gameObject.transform.localPosition.x));
        */
        Debug.Log(string.Format("Hit {2} at (x, y): ({0}, {1})", Mathf.Round(x), Mathf.Round(y), result.gameObject.name));
    }

    void Update()
    {
        if(currentTool == Level.Tool.Eraser || currentTool == Level.Tool.Brush ) { 
        if (Input.GetMouseButton(0))
            {
            ShootRaysAndGetCoordinates();
            }
        }
        //Check if the left Mouse button is clicked
        else if (Input.GetMouseButtonUp(0))
        {
            ShootRaysAndGetCoordinates();
        }
    }

    private void ShootRaysAndGetCoordinates()
    {
        //Set up the new Pointer Event
        m_PointerEventData = new PointerEventData(m_EventSystem);
        //Set the Pointer Event Position to that of the mouse position
        m_PointerEventData.position = Input.mousePosition;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        m_Raycaster.Raycast(m_PointerEventData, results);

        //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
        foreach (RaycastResult result in results)
        {
            GetLocalUICoordinates(result);
        }
    }
}
