﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DogHandler : MonoBehaviour
{
    float latestAffinityRemembered = 100.0f;
    Transform initialPosition;
    Image dogImage;
    int runningIndex = 0;
    AudioSource audioSource;

    public Sprite happySprite;
    public Sprite winSprite;
    public Sprite guiltySprite;
    public Sprite worriedSprite;
    public Sprite[] runningSprites;

    public AudioClip happySound;
    public AudioClip winSound;
    public AudioClip guiltySound;
    public AudioClip worriedSound;
    public AudioClip runningSound;
    public AudioClip givenTreatSound;

    private State dogState { get; set; }

    enum State
    {
        Worried,
        Win,
        Happy,
        Guilty,
        GivenTreat,
        Running,
    }

    void OnEnable()
    {
        dogImage = GetComponent<Image>();
        audioSource = GetComponent<AudioSource>();
        Countdown.OnLoseLevel += HandleLevelLost;
        Level.OnWinLevel += HandleLevelWon;
        Countdown.OnCloseToLosing += HandleLevelGettingWorrying;
        DrawMe.OnAffinityUpdated += HandleAffinityChange;
        GraphicRaycasterRaycasterExample.OnRaycastedDog += HandleDogGettingClicked;
    }


    void OnDisable()
    {
        Countdown.OnLoseLevel -= HandleLevelLost;
        Level.OnWinLevel -= HandleLevelWon;
        Countdown.OnCloseToLosing -= HandleLevelGettingWorrying;
        DrawMe.OnAffinityUpdated -= HandleAffinityChange;
        GraphicRaycasterRaycasterExample.OnRaycastedDog -= HandleDogGettingClicked;


    }
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        dogImage = GetComponent<Image>();
        initialPosition = gameObject.transform;
    }

    private void HandleDogGettingClicked(Vector2 pos)
    {
        ChangeToGivenTreat();
    }
    private void HandleLevelWon(float affinity)
    {
        ChangeToWin();
    }

    private void HandleLevelGettingWorrying()
    {
        ChangeToWorried();
    }

    private void HandleAffinityChange(float factor)
    {
        float difference = factor - latestAffinityRemembered;
        bool isGood = difference >= 0;
        bool isBigChange = Math.Abs(difference) > 10;   
        if (isGood)
        {
            if (isBigChange)
            {
                ChangeToHappy();
            }
        }
        else
        {
            ChangeToGuilty();
        }


        latestAffinityRemembered = factor;
    }

    private void HandleLevelLost()
    {
        //throw new NotImplementedException();
    }



    void ChangeToWin()
    {
        if (dogState == State.Win) return;
        dogState = State.Win;

        if (winSprite != null) dogImage.sprite = winSprite;
        audioSource.PlayOneShot(this.winSound);
    }
    void ChangeToHappy()
    {
        if (dogState == State.Happy) return;
        dogState = State.Happy;

        if (happySprite != null)  dogImage.sprite = happySprite;
        audioSource.PlayOneShot(this.happySound);

    }
    void ChangeToWorried()
    {
        if (dogState == State.Worried) return;
        dogState = State.Worried;

        if (worriedSprite != null) dogImage.sprite = worriedSprite;
        audioSource.PlayOneShot(this.worriedSound);
        dogState = State.Worried;
    }



    void ChangeToGuilty()
    {
        if (dogState == State.Guilty) return;
        dogState = State.Guilty;

        dogImage.sprite = guiltySprite;
        audioSource.PlayOneShot(this.guiltySound);
    }

    void ChangeToGivenTreat()
    {
        //if (dogState == State.GivenTreat) return;
        dogState = State.GivenTreat;

        gameObject.transform.position = initialPosition.position;
        if(!audioSource.isPlaying) audioSource.PlayOneShot(this.givenTreatSound);
    }
    void ChangeToNextRunning()
    {
        if (dogState == State.Running) return;
        dogState = State.Running;

        float newX = initialPosition.position.x + 500;
        float newY = initialPosition.position.y + 500;
        float newZ = initialPosition.position.z;

        runningIndex += 1;
        if (runningIndex >= runningSprites.Length)
        {
            runningIndex = 0;
        }

        gameObject.transform.position.Set(newX, newY, newZ);
        dogImage.sprite = runningSprites[runningIndex];

        audioSource.PlayOneShot(this.runningSound);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Q))
        {
            ChangeToGuilty();
        }
        else if (Input.GetKeyUp(KeyCode.W))
        {
            ChangeToHappy();
        }
        else if (Input.GetKeyUp(KeyCode.E))
        {
            ChangeToWorried();
        }
        else if (Input.GetKeyUp(KeyCode.R))
        {
            ChangeToWin();
        }
        else if (Input.GetKeyUp(KeyCode.T))
        {
            ChangeToNextRunning();
        }
        else if (Input.GetKeyUp(KeyCode.Y))
        {
            ChangeToGivenTreat();
        }
    }
}
